-- 1) Which staff members made the highest revenue for each store and deserve a bonus for the year 2017?
WITH revenue_by_staff AS (
    SELECT s.store_id, s.staff_id, CONCAT(s.first_name, ' ', s.last_name) AS staff_name, SUM(p.amount) AS total_revenue
    FROM payment p
    JOIN staff s ON p.staff_id = s.staff_id
    WHERE p.payment_date >= '2017-01-01' AND p.payment_date < '2018-01-01'
    GROUP BY s.store_id, s.staff_id
)
SELECT rbs.store_id, rbs.staff_name, rbs.total_revenue AS revenue
FROM revenue_by_staff rbs
JOIN (
    SELECT store_id, MAX(total_revenue) AS max_revenue
    FROM revenue_by_staff
    GROUP BY store_id
) AS max_revenue_per_store ON rbs.store_id = max_revenue_per_store.store_id AND rbs.total_revenue = max_revenue_per_store.max_revenue;

--2) Which five movies were rented more than the others, and what is the expected age of the audience for these movies?
SELECT title, rental_count, AVG(rental_duration) AS expected_age
FROM (
    SELECT f.title, COUNT(*) AS rental_count, f.rental_duration,
           RANK() OVER (ORDER BY COUNT(*) DESC) AS rank
    FROM film f
    JOIN inventory i ON f.film_id = i.film_id
    JOIN rental r ON i.inventory_id = r.inventory_id
    GROUP BY f.film_id, f.title, f.rental_duration
) AS ranked_films
WHERE rank <= 5
GROUP BY title, rental_count
ORDER BY rental_count DESC;

--3) Which actors/actresses didn't act for a longer period of time than the others?
SELECT actor_name, years_since_last_movie
FROM (
    SELECT CONCAT(a.first_name, ' ', a.last_name) AS actor_name,
           MAX(f.release_year) - MIN(f.release_year) AS years_since_last_movie,
           RANK() OVER (ORDER BY MAX(f.release_year) - MIN(f.release_year) DESC) AS rank
    FROM actor a
    JOIN film_actor fa ON a.actor_id = fa.actor_id
    JOIN film f ON fa.film_id = f.film_id
    GROUP BY a.actor_id
) AS actor_years
WHERE rank = 1;

